package kade.com.submission2.view

import kade.com.submission2.data.Team

interface TeamDetailView {
    fun showLoading()
    fun hideLoading()
    fun showTeamDetail(data: List<Team>)
}