package kade.com.submission2

import android.content.Context
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kade.com.submission2.adapter.FavoriteEventsAdapter
import kade.com.submission2.sqlite.FavEvents
import kade.com.submission2.sqlite.Favorite
import kade.com.submission2.sqlite.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteEventsFragment : Fragment(), AnkoComponent<Context> {

    private var favorites: MutableList<FavEvents> = mutableListOf()
    private lateinit var adapter: FavoriteEventsAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = "Favorites"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        adapter = FavoriteEventsAdapter(favorites) {
            ctx.startActivity<EventDetailActivity>(
                    //TODO : panggil parameter lain dari FavEvents database. GK, DEF, MDF,
                    //TODO : disamain kek cara ambil di MainFragment
                    "type" to "fav",
                    "id" to "${it.eventId}",
                    "eventDate" to "${it.eventDate}",
                    "eventName" to "${it.eventName}",

                    "teamAwayId" to "${it.teamAwayId}",
                    "teamAwayName" to "${it.teamAwayName}",
                    "teamAwayScore" to "${it.teamAwayScore}",

                    "teamHomeId"    to "${it.teamHomeId}",
                    "teamHomeName" to "${it.teamHomeName}",
                    "teamHomeScore" to "${it.teamHomeScore}"
            )
        }

        listEvent.adapter = adapter
        showFavorite()
        swipeRefresh.onRefresh {
            favorites.clear()
            showFavorite()
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                listEvent = recyclerView {
                    lparams (width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }
            }
        }
    }
    private fun showFavorite(){
        try {
            context?.database?.use {
                swipeRefresh.isRefreshing = false
                val result = select(FavEvents.TABLE)
                val favorite = result.parseList(classParser<FavEvents>())
                favorites.addAll(favorite)
                Log.d("FTF","data : "+favorite)
                adapter.notifyDataSetChanged()
            }
        } catch (e : SQLiteException){
            Log.d("FTF","error : "+e.toString())
        }
    }
}