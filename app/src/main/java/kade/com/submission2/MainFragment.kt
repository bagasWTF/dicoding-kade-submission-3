package kade.com.submission2

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import kade.com.submission2.adapter.MainAdapter
import kade.com.submission2.data.EventModel
import kade.com.submission2.presenter.MainPresenter
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.invisible
import kade.com.submission2.utils.visible
import kade.com.submission2.view.MainUI
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.*


class MainFragment : Fragment(), MainUI, AnkoComponent<Context> {

    private var eventMatch: MutableList<EventModel> = mutableListOf()
    private lateinit var presenter: MainPresenter
    private lateinit var adapter: MainAdapter
    private lateinit var listTeam: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var spinner: Spinner
    private lateinit var eventName: String
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MainAdapter(ctx,eventMatch){
            startActivity<EventDetailActivity>(
                    "type" to "main",
                    "id" to ""+it.eventId,
                    "eventName" to ""+it.eventName,
                    "eventDate" to ""+it.eventDate,

                    "teamAwayId" to it.teamAwayId,
                    "teamHomeId" to it.teamHomeId,
                    "teamAwayName" to it.teamAwayName,
                    "teamHomeName" to it.teamHomeName,
                    "teamHomeScore" to it.teamHomeScore,
                    "teamAwayScore" to it.teamAwayScore,
                    "teamHomeShots" to it.teamHomeShots,
                    "teamAwayShots" to it.teamAwayShots,

                    "teamHomeGoalKeeper" to it.teamHomeGoalKeeper,
                    "teamAwayGoalKeeper" to it.teamAwayGoalKeeper,
                    "teamHomeDefense" to it.teamHomeDefense,
                    "teamAwayDefense" to it.teamAwayDefense,
                    "teamHomeMidField" to it.teamHomeMidField,
                    "teamAwayMidField" to it.teamAwayMidField,
                    "teamHomeForward" to it.teamHomeForward,
                    "teamAwayForward" to it.teamAwayForward,
                    "teamHomeSubstitutes" to it.teamHomeSubstitutes,
                    "teamAwaySubstitutes" to it.teamAwaySubstitutes
            ) //
        }
        listTeam.adapter = adapter
        val request = ApiRepo()
        val gson = Gson()
        presenter = MainPresenter(this, request, gson)
        val spinnerItems = resources.getStringArray(R.array.event_match)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                eventName = spinner.selectedItem.toString()
                when(eventName){
                    spinnerItems[0] -> presenter.getNextMatchList(BuildConfig.LEAGUE_ID)
                    spinnerItems[1] -> presenter.getLastMatchList(BuildConfig.LEAGUE_ID)
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
        swipeRefresh.onRefresh {
            when(eventName){
                spinnerItems[0] -> presenter.getNextMatchList(BuildConfig.LEAGUE_ID)
                spinnerItems[1] -> presenter.getLastMatchList(BuildConfig.LEAGUE_ID)
            }

        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){

        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(8)
            backgroundColor  = R.color.colorWhitesmoke

            spinner = spinner ()
            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout{
                    //                    lparams (width = matchParent, height = wrapContent)

                    listTeam = recyclerView {
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }
        }

        // Initialization

    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun updateEventMatch(data: List<EventModel>) {
        swipeRefresh.isRefreshing = false
        eventMatch.clear()
        eventMatch.addAll(data)
        adapter.notifyDataSetChanged()
    }
}