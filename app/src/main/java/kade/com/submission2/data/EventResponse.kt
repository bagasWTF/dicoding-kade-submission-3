package kade.com.submission2.data

data class EventResponse(
        val events: List<EventModel>)