package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.data.EventResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.view.EventDetailUI
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.data.TeamResponse
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EventDetailPresenter(private val view: EventDetailUI,
                           private val apiRepository: ApiRepo,
                           private val gson: Gson) {
    fun getTeamAway(teamAwayId: String?) {
        view.showLoading()
        doAsync {
            var awayTeam = gson.fromJson(apiRepository
                    .doRequest(TSDBApi.getTeamDetail(teamAwayId)),
                    TeamResponse::class.java)

            uiThread {
                view.hideLoading()
                view.showTeamAway(awayTeam.teams)
            }
        }
    }
    fun getTeamHome(teamHomeId: String?) {
        view.showLoading()
        doAsync {

            var homeTeam = gson.fromJson(apiRepository
                    .doRequest(TSDBApi.getTeamDetail(teamHomeId)),
                    TeamResponse::class.java)

            uiThread {
                view.hideLoading()
                view.showTeamHome(homeTeam.teams)
            }
        }
    }

//TODO : apakah method ini perlu?
//    fun getTeamHomeDetail(teamHomeId: String?) {
//        view.showLoading()
//        doAsync {
//
//            var homeTeam = gson.fromJson(apiRepository
//                    .doRequest(TSDBApi.getTeamDetail(teamHomeId)),
//                    EventResponse::class.java)
//
//            uiThread {
//                view.hideLoading()
//                view.showTeamHomeDetail(homeTeam.events)
//            }
//        }
//    }
//    fun getTeamAwayDetail(teamAwayId: String?) {
//        view.showLoading()
//        doAsync {
//            var awayTeam = gson.fromJson(apiRepository
//                    .doRequest(TSDBApi.getTeamDetail(teamAwayId)),
//                    EventResponse::class.java)
//
//            uiThread {
//                view.hideLoading()
//                view.showTeamAwayDetail(awayTeam.events)
//            }
//        }
//    }
}