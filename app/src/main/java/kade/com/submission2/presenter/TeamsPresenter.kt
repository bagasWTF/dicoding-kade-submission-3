package kade.com.submission2.presenter

import android.util.Log
import com.google.gson.Gson
import kade.com.submission2.view.TeamsView
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.TSDBApi
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamsPresenter (private val view: TeamsView,
                      private val apiRepository: ApiRepo,
                      private val gson: Gson) {

    fun getTeamList(league: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TSDBApi.getTeams(league)),
                    TeamResponse::class.java
            )

            uiThread {
                view.hideLoading()
                Log.d("UIThread","output : "+data)
                view.showTeamList(data.teams)
            }
        }
    }
}