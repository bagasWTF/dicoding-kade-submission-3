package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.view.TeamDetailView
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.TSDBApi
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamDetailPresenter(private val view: TeamDetailView,
                          private val apiRepository: ApiRepo,
                          private val gson: Gson) {

    fun getTeamDetail(teamId: String) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TSDBApi.getTeamDetail(teamId)),
                    TeamResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTeamDetail(data.teams)
            }
        }
    }
}