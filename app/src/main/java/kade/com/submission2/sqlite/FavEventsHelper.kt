package kade.com.submission2.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import org.jetbrains.anko.db.*


//FavoriteEvents.db
class FavEventsHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavoriteEvents.db", null, 1) {
    companion object {
        private var instance: FavEventsHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): FavEventsHelper {
            if (instance == null) {
                instance = FavEventsHelper(ctx.applicationContext)
            }
            return instance as FavEventsHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable(FavEvents.TABLE, true,
                FavEvents.ID           to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                FavEvents.EVENT_ID              to TEXT + UNIQUE,
                FavEvents.EVENT_DATE            to TEXT,
                FavEvents.EVENT_TEAM_AWAY_ID    to TEXT,

                FavEvents.EVENT_TEAM_AWAY_NAME  to TEXT,
                FavEvents.EVENT_TEAM_AWAY_SCORE to TEXT,
                FavEvents.EVENT_TEAM_AWAY_GK    to TEXT,
                FavEvents.EVENT_TEAM_AWAY_DEF   to TEXT,
                FavEvents.EVENT_TEAM_AWAY_FWD   to TEXT,
                FavEvents.EVENT_TEAM_AWAY_MDF   to TEXT,
                FavEvents.EVENT_TEAM_AWAY_SUB   to TEXT,

                FavEvents.EVENT_TEAM_HOME_ID    to TEXT,
                FavEvents.EVENT_TEAM_HOME_NAME  to TEXT,
                FavEvents.EVENT_TEAM_HOME_SCORE to TEXT,
                FavEvents.EVENT_TEAM_HOME_GK    to TEXT,
                FavEvents.EVENT_TEAM_HOME_DEF   to TEXT,
                FavEvents.EVENT_TEAM_HOME_FWD   to TEXT,
                FavEvents.EVENT_TEAM_HOME_MDF   to TEXT,
                FavEvents.EVENT_TEAM_HOME_SUB   to TEXT
                )
        Log.d("FTF","sqlite created!")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //TODO : cek penyebab can not insert data apakah onUpgrade ?
        // Here you can upgrade tables, as usual
//        db.dropTable(Favorite.TABLE_FAVORITE, true)
//        db.dropTable(FavEvents.TABLE, true)
    }
}

// Access property for Context
val Context.database: FavEventsHelper
    get() = FavEventsHelper.getInstance(applicationContext)
