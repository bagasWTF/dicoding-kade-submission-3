package kade.com.submission2.sqlite

data class FavEvents(
            val id: Long?,
            val eventId: String?,
            val eventName: String?,
            val eventDate: String?,

            val teamHomeId: String?,
            val teamHomeName: String?,
            val teamHomeScore: String?,
            val teamHomeGoalKeeper: String?,
            val teamHomeMidField: String?,
            val teamHomeDefense: String?,
            val teamHomeForward: String?,
            val teamHomeSubstitutes: String?,

            val teamAwayId: String?,
            val teamAwayName: String?,
            val teamAwayScore: String?,
            val teamAwayGoalKeeper: String?,
            val teamAwayMidField: String?,
            val teamAwayDefense: String?,
            val teamAwayForward: String?,
            val teamAwaySubstitutes: String?
    ) {
        companion object {
            const val TABLE: String                 = "TABLE_FAVORITE_EVENTS"
            const val ID: String                    = "ID_"
            const val EVENT_ID: String              = "EVENT_ID"
            const val EVENT_NAME: String            = "EVENT_NAME"
            const val EVENT_DATE: String            = "EVENT_DATE"

            const val EVENT_TEAM_HOME_ID: String    = "EVENT_TEAM_HOME_ID"
            const val EVENT_TEAM_HOME_NAME: String  = "EVENT_TEAM_HOME_NAME"
            const val EVENT_TEAM_HOME_SCORE: String = "EVENT_TEAM_HOME_SCORE"
            const val EVENT_TEAM_HOME_GK: String    = "EVENT_TEAM_HOME_GK"
            const val EVENT_TEAM_HOME_DEF: String   = "EVENT_TEAM_HOME_DEF"
            const val EVENT_TEAM_HOME_MDF: String   = "EVENT_TEAM_HOME_MDF"
            const val EVENT_TEAM_HOME_FWD: String   = "EVENT_TEAM_HOME_FWD"
            const val EVENT_TEAM_HOME_SUB: String   = "EVENT_TEAM_HOME_SUB"

            const val EVENT_TEAM_AWAY_ID: String    = "EVENT_TEAM_AWAY_ID"
            const val EVENT_TEAM_AWAY_NAME: String  = "EVENT_TEAM_AWAY_NAME"
            const val EVENT_TEAM_AWAY_SCORE: String = "EVENT_TEAM_AWAY_SCORE"
            const val EVENT_TEAM_AWAY_GK: String    = "EVENT_TEAM_AWAY_GK"
            const val EVENT_TEAM_AWAY_DEF: String   = "EVENT_TEAM_AWAY_DEF"
            const val EVENT_TEAM_AWAY_MDF: String   = "EVENT_TEAM_AWAY_MDF"
            const val EVENT_TEAM_AWAY_FWD: String   = "EVENT_TEAM_AWAY_FWD"
            const val EVENT_TEAM_AWAY_SUB: String   = "EVENT_TEAM_AWAY_SUB"
    }
}