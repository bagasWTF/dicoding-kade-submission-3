package kade.com.submission2

import android.Manifest
import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.annotation.RequiresPermission
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kade.com.submission2.data.EventModel
import kade.com.submission2.data.Team
import kade.com.submission2.presenter.EventDetailPresenter
import kade.com.submission2.sqlite.FavEvents
import kade.com.submission2.sqlite.database
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.invisible
import kade.com.submission2.utils.visible
import kade.com.submission2.view.EventDetailUI
import org.jetbrains.anko.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.design.snackbar

class EventDetailActivity :  AppCompatActivity(), EventDetailUI {

    private lateinit var presenter: EventDetailPresenter

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var scrollView: ScrollView
    private lateinit var teamAwayId:String
    private lateinit var teamHomeId:String
    private lateinit var teamAwayName:String
    private lateinit var teamHomeName:String
    private lateinit var teamAwayScore:String
    private lateinit var teamHomeScore:String
    private lateinit var events:EventModel

    private lateinit var teamHomeGoalKeeper:String
    private lateinit var teamHomeDefense:String
    private lateinit var teamHomeMidField:String
    private lateinit var teamHomeForward:String
    private lateinit var teamHomeSubstitutes:String

    private lateinit var teamAwayGoalKeeper:String
    private lateinit var teamAwayDefense:String
    private lateinit var teamAwayMidField:String
    private lateinit var teamAwayForward:String
    private lateinit var teamAwaySubstitutes:String

    private lateinit var tipeGetData:String
    private lateinit var id:String
    private lateinit var eventId:String
    private lateinit var eventName:String
    private lateinit var eventDate:String
    private lateinit var teamAwayImg : ImageView
    private lateinit var teamHomeImg: ImageView

    private lateinit var progressBar: ProgressBar

    private fun teamSplitter(data:String?) : String {
        var dataSplit = data.toString().split(";")
        var textStr:String = ""
//        if (dataSplit.size>1){
            for (x in 0..(dataSplit.size-2)){
                textStr += dataSplit[x] +"\n"
            }
//        }
        return textStr
    }
    private fun dateformat(data: String?) : String {
        var dataInput : String = data.toString()
        var arrData = dataInput.split("-")
        var bulan : String?
        when(arrData[1]){
            "01"-> bulan = "Jan"
            "02"-> bulan = "Feb"
            "03"-> bulan = "Mar"
            "04"-> bulan = "Apr"
            "05"-> bulan = "Mei"
            "06"-> bulan = "Jun"
            "07"-> bulan = "Jul"
            "08"-> bulan = "Aug"
            "09"-> bulan = "Sep"
            "10"-> bulan = "Oct"
            "11"-> bulan = "Nov"
            "12"-> bulan = "Dec"
            else->{
                bulan = "-"
            }
        }
        return arrData[2]+" "+bulan+" "+arrData[0]
    }
    @RequiresPermission(value = Manifest.permission.ACCESS_NETWORK_STATE)
    fun Context.isConnected(): Boolean {
        val connectivityManager = this
                .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        connectivityManager?.let {
            val netInfo = it.activeNetworkInfo
            netInfo?.let {
                if (it.isConnected) return true
            }
        }
        return false
    }
    private fun initData(){
        eventName = intent.getStringExtra("eventName")
        eventDate = dateformat(intent.getStringExtra("eventDate"))
        eventId = id
        teamAwayId = intent.getStringExtra("teamAwayId")
        teamHomeId = intent.getStringExtra("teamHomeId")
        teamAwayName = intent.getStringExtra("teamAwayName")
        teamHomeName = intent.getStringExtra("teamHomeName")

        if(intent.getStringExtra("teamHomeScore").isNullOrBlank())          teamHomeScore = "-"         else teamHomeScore =intent.getStringExtra("teamHomeScore")
        if(intent.getStringExtra("teamAwayScore").isNullOrBlank())          teamAwayScore = "-"         else teamAwayScore = intent.getStringExtra("teamAwayScore")

        if(intent.getStringExtra("teamHomeGoalKeeper").isNullOrBlank())     teamHomeGoalKeeper = "-"    else teamHomeGoalKeeper = teamSplitter(intent.getStringExtra("teamHomeGoalKeeper"))
        if(intent.getStringExtra("teamHomeDefense").isNullOrBlank())        teamHomeDefense = "-"       else teamHomeDefense = teamSplitter(intent.getStringExtra("teamHomeDefense"))
        if(intent.getStringExtra("teamHomeMidField").isNullOrBlank())       teamHomeMidField = "-"      else teamHomeMidField = teamSplitter(intent.getStringExtra("teamHomeMidField"))
        if(intent.getStringExtra("teamHomeForward").isNullOrBlank())        teamHomeForward = "-"       else teamHomeForward = teamSplitter(intent.getStringExtra("teamHomeForward"))
        if(intent.getStringExtra("teamHomeSubstitutes").isNullOrBlank())    teamHomeSubstitutes = "-"   else teamHomeSubstitutes =teamSplitter(intent.getStringExtra("teamHomeSubstitutes"))

        if(intent.getStringExtra("teamAwayGoalKeeper").isNullOrBlank())     teamAwayGoalKeeper = "-"    else {teamAwayGoalKeeper = teamSplitter(intent.getStringExtra("teamAwayGoalKeeper"))}
            if(intent.getStringExtra("teamAwayDefense").isNullOrBlank())    teamAwayDefense = "-"       else {teamAwayDefense = teamSplitter(intent.getStringExtra("teamAwayDefense")) }
        if(intent.getStringExtra("teamAwayMidField").isNullOrBlank())       teamAwayMidField = "-"      else {teamAwayMidField = teamSplitter(intent.getStringExtra("teamAwayMidField")) }
        if(intent.getStringExtra("teamAwayForward").isNullOrBlank())        teamAwayForward = "-"       else {teamAwayForward = teamSplitter(intent.getStringExtra("teamAwayForward")) }
        if(intent.getStringExtra("teamAwaySubstitutes").isNullOrBlank())    teamAwaySubstitutes = "-"   else {teamAwaySubstitutes =teamSplitter(intent.getStringExtra("teamAwaySubstitutes")) }

        Log.d("FTF","data : masuk initData")
    }

    private fun favData(){
        Log.d("FTF","data : masuk favData")
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        var data:Intent = intent
        //TODO : pengecekan type dari homeactivity main = initData, fav = favorit
        //TODO : if fav, ambil dta dari sqlite
        tipeGetData = intent.getStringExtra("type")
        id          = intent.getStringExtra("id")

        if      (tipeGetData.equals("main")){initData()}
        else if (tipeGetData.equals("fav")) {favData()}

        scrollView = scrollView {
            id = R.id.scroll_view
            lparams(matchParent, wrapContent)

            linearLayout{
                lparams(matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL
                linearLayout {
                    lparams(matchParent, wrapContent){
                        leftPadding = dip(12)
                        rightPadding = dip(12)
                    }
                    orientation = LinearLayout.VERTICAL
                    progressBar = progressBar {
                    }.lparams{
                        gravity = Gravity.CENTER_HORIZONTAL
                        bottomPadding = dip(12); topPadding = dip(12)
                    }
                    textView { // Date Event
                        id = R.id.event_date
                        text = eventDate
                        textSize = 24f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent){
                        bottomPadding = dip(5); topPadding = dip(5)
                    }.setTypeface(Typeface.DEFAULT_BOLD)

                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        linearLayout {
                            //TEAM HOME
                            textView {
                                id = R.id.team_home_name
                                textSize = 20f
                                text = teamHomeName
                                textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            }.lparams(matchParent, wrapContent){
                                bottomPadding = dip(8)
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight=1f
                        }

                        linearLayout {
                            //TEAM AWAY
                            textView {
                                textSize = 20f
                                id = R.id.team_away_name
                                text = teamAwayName
                                textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            }.lparams(matchParent, wrapContent){
                                bottomPadding = dip(8)
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight=1f
                        }
                    }.lparams (matchParent, wrapContent)
                    linearLayout {
                        lparams(matchParent, wrapContent)
                        orientation = LinearLayout.HORIZONTAL
                        //TEAM HOME
                        linearLayout {
                            orientation = LinearLayout.VERTICAL

                            teamHomeImg = imageView { // Team Home
//                                layoutParams = LinearLayout.LayoutParams(dip(64),dip(64))
                            }.lparams(dip(64),dip(64)){
                                gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView {
                                id = R.id.team_home_score
                                text = teamHomeScore
                                textSize = 24f
                            }.lparams{
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight = 2f
                        }

                        //VS
                        linearLayout {
                            textView {
                                text = "VS"
                                textSize = 16f
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                                gravity = Gravity.CENTER
                            }.lparams(matchParent, matchParent)
                        }.lparams(0, matchParent){
                            weight = 1f
                        }

                        //TEAM AWAY
                        linearLayout {
                            orientation = LinearLayout.VERTICAL
                            teamAwayImg = imageView { // Team Away
//                                layoutParams = LinearLayout.LayoutParams(dip(64),dip(64))
                            }.lparams(dip(64),dip(64)){
                                gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView {
                                id = R.id.team_away_score
                                text = teamAwayScore
                                textSize = 24f
                            }.lparams{
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight = 2f
                        }
                    } // HORIZONTAL

                    //GoalKeeper
                    textView {
                        text = "Goal Keeper"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            id = R.id.team_away_gk
                            text = teamAwayGoalKeeper
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            id = R.id.team_home_gk
                            text = teamHomeGoalKeeper
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //Defense
                    textView {
                        text = "Defense"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            id = R.id.team_away_def
                            text = teamAwayDefense
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            id = R.id.team_home_def
                            text = teamHomeDefense
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //MidField
                    textView {
                        text = "MidField"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            id = R.id.team_away_mdf
                            text = teamAwayMidField
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            id = R.id.team_home_mdf
                            text = teamHomeMidField
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //Forward
                    textView {
                        text = "Forward"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            id = R.id.team_away_fwd
                            text = teamAwayForward
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            id = R.id.team_home_fwd
                            text = teamHomeForward
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //Substitutes
                    textView {
                        text = "Substitutes"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            id = R.id.team_away_sub
                            text = teamAwaySubstitutes
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            id = R.id.team_home_sub
                            text = teamHomeSubstitutes
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                } //VERTIKAL
            }
        } // SCROLL VIEW
        val request = ApiRepo()
        val gson = Gson()

//        favoriteState()
        Log.d("FTF","teamHomeId : "+teamHomeId+", teamAwayId : "+teamAwayId)
        presenter = EventDetailPresenter(this, request, gson)
        if (isConnected()){
            //TODO : ambil data URL gambar ke showTeamAway & showTeamHome
            presenter.getTeamAway(teamAwayId)
            presenter.getTeamHome(teamHomeId)

//            presenter.getTeamAwayDetail(intent.getStringExtra("teamAwayId"))
//            presenter.getTeamHomeDetail(intent.getStringExtra("teamHomeId"))
        }
//        try {
//            database.use {
//                insert(FavEvents.TABLE,
//                        FavEvents.EVENT_ID      to ""+1234,
//                        FavEvents.EVENT_DATE            to ""+eventDate,
//                        FavEvents.EVENT_NAME            to ""+eventName,
//
//                        FavEvents.EVENT_TEAM_AWAY_ID    to ""+teamAwayId,
//                        FavEvents.EVENT_TEAM_AWAY_NAME  to ""+teamAwayName,
//                        FavEvents.EVENT_TEAM_AWAY_SCORE to ""+teamAwayScore,
//                        FavEvents.EVENT_TEAM_AWAY_GK    to ""+teamAwayGoalKeeper,
//                        FavEvents.EVENT_TEAM_AWAY_DEF   to ""+teamAwayDefense,
//                        FavEvents.EVENT_TEAM_AWAY_FWD   to ""+teamAwayForward,
//                        FavEvents.EVENT_TEAM_AWAY_MDF   to ""+teamAwayMidField,
//                        FavEvents.EVENT_TEAM_AWAY_SUB   to ""+teamAwaySubstitutes,
//
//                        FavEvents.EVENT_TEAM_HOME_ID    to ""+teamHomeId,
//                        FavEvents.EVENT_TEAM_HOME_NAME  to ""+teamHomeName,
//                        FavEvents.EVENT_TEAM_HOME_SCORE to ""+teamHomeScore,
//                        FavEvents.EVENT_TEAM_HOME_GK    to ""+teamHomeGoalKeeper,
//                        FavEvents.EVENT_TEAM_HOME_DEF   to ""+teamHomeDefense,
//                        FavEvents.EVENT_TEAM_HOME_FWD   to ""+teamHomeForward,
//                        FavEvents.EVENT_TEAM_HOME_MDF   to ""+teamHomeMidField,
//                        FavEvents.EVENT_TEAM_HOME_SUB   to ""+teamHomeSubstitutes
//
//                )
//            }
////            favoriteState()
//            snackbar(scrollView , "Added to favorite "+eventId+", favState : "+isFavorite).show()
//        } catch (e: SQLiteException){
//            Log.d("FTF","error : "+e.toString())
////            snackbar(scrollView , e.localizedMessage).show()
//        }

    }
    override fun showTeamAway(awayTeam: List<Team>) {
        Glide.with(getBaseContext()).load(awayTeam.get(0).teamBadge).into(teamHomeImg)
    }

    override fun showTeamHome(homeTeam: List<Team>) {
        Glide.with(getBaseContext()).load(homeTeam.get(0).teamBadge ).into(teamAwayImg )
    }

//    override fun showTeamAwayDetail(awayTeamDet: List<EventModel>?) {
//        //TODO : apakah fungsi ini perlu?
//        findViewById<TextView>(R.id.team_home_gk).setText(awayTeamDet?.get(0)?.teamAwayGoalKeeper)
//        findViewById<TextView>(R.id.team_home_def).setText(awayTeamDet?.get(0)?.teamAwayDefense)
//        findViewById<TextView>(R.id.team_home_mdf).setText(awayTeamDet?.get(0)?.teamAwayMidField)
//        findViewById<TextView>(R.id.team_home_fwd).setText(awayTeamDet?.get(0)?.teamAwayForward)
//        findViewById<TextView>(R.id.team_home_sub).setText(awayTeamDet?.get(0)?.teamAwaySubstitutes)
//    }
//
//    override fun showTeamHomeDetail(homeTeamDet: List<EventModel>?) {
//        //TODO : apakah fungsi ini perlu?
//        findViewById<TextView>(R.id.team_home_gk).setText(homeTeamDet?.get(0)?.teamHomeGoalKeeper)
//        findViewById<TextView>(R.id.team_home_def).setText(homeTeamDet?.get(0)?.teamHomeDefense)
//        findViewById<TextView>(R.id.team_home_mdf).setText(homeTeamDet?.get(0)?.teamHomeMidField)
//        findViewById<TextView>(R.id.team_home_fwd).setText(homeTeamDet?.get(0)?.teamHomeForward)
//        findViewById<TextView>(R.id.team_home_sub).setText(homeTeamDet?.get(0)?.teamHomeSubstitutes)
//    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
//                addToFavorite()
                isFavorite = !isFavorite
                setFavorite()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

//    override fun showTeamDetail(data: List<Team>) {
//        teams = Team(data[0].teamId,
//                data[0].teamName,
//                data[0].teamBadge)
//        swipeRefresh.isRefreshing = false
////        Picasso.get().load(data[0].teamBadge).into(teamBadge)sss
//        Glide.with(getBaseContext()).load(data[0].teamBadge).into(teamBadge)
//        teamName.text = data[0].teamName
//        teamDescription.text = data[0].teamDescription
//        teamFormedYear.text = data[0].teamFormedYear
//        teamStadium.text = data[0].teamStadium
//    }
    private fun addToFavorite(){
        try {
            database.use {
                insert(FavEvents.TABLE,
                        FavEvents.EVENT_ID      to eventId,
                        FavEvents.EVENT_DATE            to eventDate,
                        FavEvents.EVENT_NAME            to eventName,

                        FavEvents.EVENT_TEAM_AWAY_ID    to teamAwayId,
                        FavEvents.EVENT_TEAM_AWAY_NAME  to teamAwayName,
                        FavEvents.EVENT_TEAM_AWAY_SCORE to teamAwayScore,
                        FavEvents.EVENT_TEAM_AWAY_GK    to teamAwayGoalKeeper,
                        FavEvents.EVENT_TEAM_AWAY_DEF   to teamAwayDefense,
                        FavEvents.EVENT_TEAM_AWAY_FWD   to teamAwayForward,
                        FavEvents.EVENT_TEAM_AWAY_MDF   to teamAwayMidField,
                        FavEvents.EVENT_TEAM_AWAY_SUB   to teamAwaySubstitutes,

                        FavEvents.EVENT_TEAM_HOME_ID    to teamHomeId,
                        FavEvents.EVENT_TEAM_HOME_NAME  to teamHomeName,
                        FavEvents.EVENT_TEAM_HOME_SCORE to teamHomeScore,
                        FavEvents.EVENT_TEAM_HOME_GK    to teamHomeGoalKeeper,
                        FavEvents.EVENT_TEAM_HOME_DEF   to teamHomeDefense,
                        FavEvents.EVENT_TEAM_HOME_FWD   to teamHomeForward,
                        FavEvents.EVENT_TEAM_HOME_MDF   to teamHomeMidField,
                        FavEvents.EVENT_TEAM_HOME_SUB   to teamHomeSubstitutes

                        )
            }
//            favoriteState()
            snackbar(scrollView , "Added to favorite "+teamAwayId+", eventName : "+teamAwayName).show()
            Log.d("FTF","teamAwayScore"+teamAwayScore+","+"teamAwayGoalKeeper"+teamAwayGoalKeeper+","+"teamAwayDefense"+teamAwayDefense+","+"teamAwayForward"+teamAwayForward+","+"teamAwayMidField"+teamAwayMidField+","+"teamAwaySubstitutes"+teamAwaySubstitutes+",")
        } catch (e: SQLiteException){
            Log.d("FTF","error : "+e.toString())
//            snackbar(scrollView , e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite(){
        try {
            database.use {
                delete(FavEvents.TABLE, FavEvents.EVENT_ID+" = {id}",
                        "id" to eventId)
            }
            snackbar(scrollView , "Removed to favorite").show()
        } catch (e: SQLiteConstraintException){
            snackbar(scrollView , e.localizedMessage).show()
        }
    }
    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }
    private fun favoriteState(){
        //TODO : sqlite.SQLiteException no such table (?)
        //TODO : apakah krn sebelumnya udah pernah bikin table yg beda?
        database.use {
            val result = select(FavEvents.TABLE)
                    .whereArgs(FavEvents.EVENT_ID+" = {id}",
                            "id" to eventId)
                val favorite = result.parseList(classParser<FavEvents>())
            Log.d("FTF","result : "+result.toString()+", data : "+favorite)
            if (!favorite.isEmpty()) isFavorite = true
        }
    }


}